package bankapp;

import org.junit.Assert;
import org.junit.Test;

import model.Account;
import model.Transfer;
import services.TransferService;
import transfers.Message;

public class TransferServiceTest {

	private static final int RESULT_WELL = 100;
	private static final String MESSAGE = "Transfer Done!";
	private static final int RESULT_WRONG = 404;
	private static final String MESSAGE_WRONG = "You exceeds the limit of a transfer!!";

	@Test
	public void testInternalTransferDone() {
		TransferService transferService = new TransferService();
		Transfer transfer = new Transfer();
		transfer.setAmount(10000);
		transfer.setDescription("test transfer");
		Account originAccount = initAccount(50000, "1465", "Cuenta Nomina");
		transfer.setOriginAccount(originAccount);
		Account finalAccount = initAccount(34003, "1465", "Cuenta Naranja");
		transfer.setFinalAccount(finalAccount);
		Message message = transferService.makeTransfer(transfer);
		Assert.assertEquals(RESULT_WELL, message.getCode());
		Assert.assertEquals(MESSAGE, message.getDescription());
	}
	
	@Test
	public void testExternalTransferDone() {
		TransferService transferService = new TransferService();
		Transfer transfer = new Transfer();
		transfer.setAmount(100);
		transfer.setDescription("test transfer");
		Account originAccount = initAccount(50000, "1465", "Cuenta Nomina");
		transfer.setOriginAccount(originAccount);
		Account finalAccount = initAccount(34003, "4300", "Cuenta Nomina");
		transfer.setFinalAccount(finalAccount);
		Message message = transferService.makeTransfer(transfer);
		Assert.assertEquals(RESULT_WELL, message.getCode());
		Assert.assertEquals(MESSAGE, message.getDescription());
	}
	
	@Test
	public void testExternalTransferFailLimit() {
		TransferService transferService = new TransferService();
		Transfer transfer = new Transfer();
		transfer.setAmount(10044);
		transfer.setDescription("test transfer");
		Account originAccount = initAccount(50000, "1465", "Cuenta Nomina");
		transfer.setOriginAccount(originAccount);
		Account finalAccount = initAccount(34003, "4300", "Cuenta Nomina");
		transfer.setFinalAccount(finalAccount);
		Message message = transferService.makeTransfer(transfer);
		Assert.assertEquals(RESULT_WRONG, message.getCode());
		Assert.assertEquals(MESSAGE_WRONG, message.getDescription());
	}

	private Account initAccount(double balance, String bankId, String description) {
		Account account = new Account();
		account.setBalance(balance);
		account.setBankIdentification(bankId);
		account.setDescription(description);
		return account;
	}

}
