package transfers;

import model.Account;
import model.Transfer;

/**
 * 
 * @author usamaalanbari
 *
 */
public interface IMakeTransfer {

	/**
	 * 
	 * @param origin
	 * @param destiny
	 * @return
	 */
	public Message makeTransfer(Transfer transfer);
}
