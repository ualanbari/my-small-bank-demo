package transfers;

import model.Transfer;

public class InternalTransfer implements IMakeTransfer {

	@Override
	public Message makeTransfer(Transfer transfer) {
		Message message = new Message();
		//call backend system to process transfer
		//Result result = BackendSystem.call(originAccount, DestinyAccount, Amount);
		//Message suppose OK result, not error managed
		message.setCode(100);
		message.setDescription("Transfer Done!");
		return message;
	}

}
