package transfers;


public class TransferFactory {
	
	public static IMakeTransfer getTypeTransfer(String bankIdOrigin, String bankIdDestiny) {
		
		if (bankIdOrigin.equals(bankIdDestiny)) {
			return new InternalTransfer();
		} else {
			return new ExternalTransfer();
		}
			
	}

}
