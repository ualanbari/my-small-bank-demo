package transfers;

import model.Transfer;

public class ExternalTransfer implements IMakeTransfer {

	private static final double LIMIT = 1000;

	@Override
	public Message makeTransfer(Transfer transfer) {
		Message message = new Message();
		if (transfer.getAmount() > LIMIT) {
			//call backend system to process transfer
			//Result result = BackendSystem.call(originAccount, DestinyAccount, Amount, commision);
			//Message suppose OK result, not error managed		
			message.setCode(404);
			message.setDescription("You exceeds the limit of a transfer!!");
		} else {
			//call backend system to process transfer
			//Result result = BackendSystem.call(originAccount, DestinyAccount, Amount, commision);
			//Message suppose OK result, not error managed		
			message.setCode(100);
			message.setDescription("Transfer Done!");
		}
		return message;
	}

}
