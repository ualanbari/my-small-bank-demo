package model;

//@Entity
public class Transfer {
	
	private int id;
	private String description;
	private Account originAccount;
	private Account finalAccount;
	private double amount;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Account getOriginAccount() {
		return originAccount;
	}
	public void setOriginAccount(Account originAccount) {
		this.originAccount = originAccount;
	}
	public Account getFinalAccount() {
		return finalAccount;
	}
	public void setFinalAccount(Account finalAccount) {
		this.finalAccount = finalAccount;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	
}
