package model;

import java.util.List;

public class Account {
	
	private long numberAccount;
	private String description;
	private double balance;
	private List<Transfer> listOftransfers;
	private String bankIdentification;
	
	public long getNumberAccount() {
		return numberAccount;
	}
	public void setNumberAccount(long numberAccount) {
		this.numberAccount = numberAccount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public List<Transfer> getListOftransfers() {
		return listOftransfers;
	}
	public void setListOftransfers(List<Transfer> listOftransfers) {
		this.listOftransfers = listOftransfers;
	}
	public String getBankIdentification() {
		return bankIdentification;
	}
	public void setBankIdentification(String bankIdentification) {
		this.bankIdentification = bankIdentification;
	}

}
