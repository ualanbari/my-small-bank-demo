package services;

import model.Transfer;
import transfers.IMakeTransfer;
import transfers.Message;
import transfers.TransferFactory;

public class TransferService {
	
	public Message makeTransfer(Transfer transfer) {
		IMakeTransfer transferType = TransferFactory.getTypeTransfer(transfer.getOriginAccount().getBankIdentification(), 
																    transfer.getFinalAccount().getBankIdentification());
		
		return transferType.makeTransfer(transfer);
	}

}
